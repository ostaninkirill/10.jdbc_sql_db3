package com.epam.mysql.dto;

import java.time.LocalDate;

public class Bid {
    private int bid_id;
    private LocalDate bid_date;
    private double bid_value;
    private int item_id;
    private int user_id;

    public Bid(int bid_id, LocalDate bid_date, double bid_value, int item_id, int user_id) {
        this.bid_id = bid_id;
        this.bid_date = bid_date;
        this.bid_value = bid_value;
        this.item_id = item_id;
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return  "id = " + bid_id +
                " || bid_date = " + bid_date +
                " || bid_value = " + bid_value +
                " || item_id = " + item_id +
                " || user_id = " + user_id;
    }



}
