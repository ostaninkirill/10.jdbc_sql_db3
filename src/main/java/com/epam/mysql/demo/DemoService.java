package com.epam.mysql.demo;


import com.epam.mysql.dao.IBidsDAO;
import com.epam.mysql.dao.IItemsDAO;
import com.epam.mysql.dao.IUsersDAO;
import com.epam.mysql.dto.Item;
import com.epam.mysql.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;


@Service
public class DemoService implements IDemoService{

    @Autowired
    private IBidsDAO bidsDAO;

    @Autowired
    private IUsersDAO usersDAO;

    @Autowired
    private IItemsDAO itemsDAO;

    @Override
    public void execute() {
        System.out.println("1. Список ставок данного пользователя");
        bidsDAO.getBidsByUserId(8).forEach(System.out::println);
        System.out.println();

        System.out.println("2. Список лотов данного пользователя");
        itemsDAO.getItemsByUserId(6).forEach(System.out::println);
        System.out.println();

        System.out.println("3. Поиск лотов по подстроке в названии");
        itemsDAO.searchItemsByName("1 рубль").forEach(System.out::println);
        System.out.println();

        System.out.println("4. Поиск лотов по подстроке в описании");
        itemsDAO.searchItemsByDescription("Екатерина 2").forEach(System.out::println);;
        System.out.println();

        System.out.println("5. Средняя цена лотов каждого пользователя");
        itemsDAO.avgPriceEachUsers().forEach(System.out::println);
        System.out.println();

        System.out.println("6. Максимальный размер имеющихся ставок на каждый лот");
        bidsDAO.maxSizeHavingBidsEachLot().forEach(System.out::println);
        System.out.println();

        System.out.println("7. Список действующих лотов данного пользователя");
        itemsDAO.listActiveUserBids(9).forEach(System.out::println);
        System.out.println();

        System.out.println("8. Добавить нового пользователя");
        usersDAO.addNewUser(new User(12,"Гаврилов Никита", "Казань, ул. Свободы 5-32", "Nikitos", "fwefw3h544574"));
        System.out.println();

        System.out.println("9. Добавить новый лот");
        itemsDAO.addNewItem(new Item(11, "КОПЕЙКА 1882 год", "МАЛОФОРМАТНАЯ", 140000, 10000,
                LocalDate.of(2019,01,01), LocalDate.of(2019,02,11), 4));
        System.out.println();

        System.out.println("10. Удалить ставки данного пользователя");
        bidsDAO.deleteBidsByUserId(4);
        System.out.println();

        System.out.println("11. Удалить лоты данного пользователя");
        itemsDAO.deleteItemsByUser(4);
        System.out.println();

        System.out.println("12. Увеличить стартовые цены товаров данного пользователя вдвое");
        itemsDAO.doubleUpStartPriceThisUser(5);
        System.out.println();

        System.out.println("13. Если инкремент меньше 300, то он будет равен 300");
        itemsDAO.bidincrementLess300();
        System.out.println();

    }
}
