package com.epam.mysql;

import com.epam.mysql.demo.IDemoService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class Application {

	public static void main(String[] args) {
		ApplicationContext context =
				new AnnotationConfigApplicationContext(AppConfig.class);
		context.getBean(IDemoService.class).execute();

	}
}

