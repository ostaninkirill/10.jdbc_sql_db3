package com.epam.mysql.dao;

import com.epam.mysql.dto.Item;

import java.util.List;
import java.util.Map;

public interface IItemsDAO {
    List<Item> getItemsByUserId (int userId);
    List<Item> searchItemsByName (String search);
    List<Item> searchItemsByDescription (String search);
    List<Map<String, Object>> avgPriceEachUsers();
    List<Item> listActiveUserBids (int userId);
    void addNewItem(Item item);
    void deleteItemsByUser(int userId);
    void doubleUpStartPriceThisUser(int userId);
    void bidincrementLess300();
}
