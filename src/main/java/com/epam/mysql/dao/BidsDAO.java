package com.epam.mysql.dao;

import com.epam.mysql.dto.Bid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class BidsDAO implements IBidsDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Bid> ROW_MAPPER = (resultSet, rowNumber) ->
    new Bid(
            resultSet.getInt("bid_id"),
            resultSet.getDate("bid_date").toLocalDate(),
            resultSet.getInt("bid_value"),
            resultSet.getInt("items_item_id"),
            resultSet.getInt("users_user_id"));

    @Override
    public List<Bid> getBidsByUserId(int userId) {
        String sql = "SELECT * FROM bids WHERE users_user_id = ?";
        return jdbcTemplate.query(sql, new Object[]{userId}, ROW_MAPPER);
    }

    @Override
    public List<Map<String, Object>> maxSizeHavingBidsEachLot() {
        String sql = "SELECT title, MAX(bid_value) " +
                "FROM internet_auction.bids, internet_auction.items " +
                "WHERE items.item_id = bids.items_item_id " +
                "GROUP BY title";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public void deleteBidsByUserId(int userId) {
        String sql = "DELETE FROM internet_auction.bids" +
                     " WHERE users_user_id = ?";
        jdbcTemplate.update(sql, userId);
    }


}
