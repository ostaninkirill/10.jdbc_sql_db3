package com.epam.mysql.dao;

import com.epam.mysql.dto.Bid;

import java.util.List;
import java.util.Map;

public interface IBidsDAO {
    List<Bid> getBidsByUserId (int userId);
    List<Map<String, Object>>maxSizeHavingBidsEachLot();
    void deleteBidsByUserId (int userId);


}
