package com.epam.mysql.dao;

import com.epam.mysql.dto.User;

public interface IUsersDAO {
    void addNewUser(User user);
}
