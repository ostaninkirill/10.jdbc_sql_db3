package com.epam.mysql.dao;

import com.epam.mysql.dto.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;


import java.sql.Date;
import java.util.List;
import java.util.Map;

@Repository
public class ItemsDAO implements IItemsDAO{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Item> ROW_MAPPER = (resultSet, rowNumber) ->
            new Item(
                    resultSet.getInt("item_id"),
                    resultSet.getString("title"),
                    resultSet.getString("description"),
                    resultSet.getDouble("start_price"),
                    resultSet.getDouble("bid_increment"),
                    resultSet.getDate("start_date").toLocalDate(),
                    resultSet.getDate("stop_date").toLocalDate(),
                    resultSet.getInt("users_user_id"));


    @Override
    public List<Item> getItemsByUserId(int userId) {
        String sql = "SELECT * FROM internet_auction.items " +
                "WHERE users_user_id = ?";
        return jdbcTemplate.query(sql, new Object[]{userId}, ROW_MAPPER);
    }

    @Override
    public List<Item> searchItemsByName(String search) {
        search = "%" + search + "%";
        String sql = "SELECT * FROM internet_auction.items " +
                "WHERE title LIKE ?";
        return jdbcTemplate.query(sql, new Object[] {search}, ROW_MAPPER);
    }

    @Override
    public List<Item> searchItemsByDescription(String search) {
        search = "%" + search + "%";
        String sql = "SELECT * FROM internet_auction.items " +
                "WHERE description LIKE ?";
        return jdbcTemplate.query(sql, new Object[] {search}, ROW_MAPPER);
    }


    @Override
    public List<Map<String, Object>> avgPriceEachUsers() {
        String sql = "SELECT full_name, AVG(start_price)" +
                " FROM internet_auction.items, internet_auction.users" +
                " WHERE items.users_user_id = users.user_id" +
                " GROUP BY full_name";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Item> listActiveUserBids(int userId) {
        String sql = "SELECT * FROM internet_auction.items " +
                "WHERE users_user_id = ? " +
                "AND CURRENT_DATE() BETWEEN start_date AND stop_date";
        return jdbcTemplate.query(sql, new Object[]{userId}, ROW_MAPPER);
    }

    @Override
    public void addNewItem(Item item) {
        String sql = "INSERT INTO internet_auction.items " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, NULL, ?)";
        jdbcTemplate.update(sql, item.getItem_id(), item.getTitle(), item.getDescription(), item.getStart_price(),
                item.getBid_increment(), Date.valueOf(item.getStart_date()), Date.valueOf(item.getStop_date()), item.getUsers_user_id());
    }

    @Override
    public void deleteItemsByUser(int userId) {
        String sql = "DELETE b " +
                "FROM internet_auction.bids AS b " +
                "INNER JOIN internet_auction.items AS i ON b.items_item_id = i.item_id " +
                "WHERE i.users_user_id = ?";
        String sql2 = "DELETE FROM internet_auction.items " +
                "WHERE users_user_id = ?";
        jdbcTemplate.update(sql, userId);
        jdbcTemplate.update(sql2, userId);
    }

    @Override
    public void doubleUpStartPriceThisUser(int userId) {
        String sql = "UPDATE internet_auction.items " +
                "SET start_price = start_price*2 " +
                "WHERE users_user_id = ?";
        jdbcTemplate.update(sql, userId);
    }

    @Override
    public void bidincrementLess300() {
        String sql = "UPDATE internet_auction.items " +
                "SET bid_increment = ? " +
                "WHERE bid_increment < 300";
        jdbcTemplate.update(sql, 300);
    }


}
